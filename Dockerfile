FROM debian:latest

RUN apt-get update \
    && apt-get upgrade -y && apt-get -y install git \
        postgresql-client python3 python3-pip net-tools nano\
    && rm -rf /var/lib/apt/lists/*

WORKDIR /var/django/app
RUN pip3 install django
RUN python3 -m django --version
RUN django-admin startproject app . 

EXPOSE 8000
CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]
